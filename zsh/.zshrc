# Init completion
autoload -U compinit && compinit
compinit

# Init custom prompt
autoload promptinit
promptinit

# Init colors for prompt
autoload -U colors && colors

# Autocompletion with menu
zstyle ':completion:*' menu select

# Custom prompt
PROMPT="%{$fg_bold[yellow]%}%T%{$reset_color%}% %{$fg_no_bold[yellow]%}%4~%{$reset_color%}%# "

# Some aliases from .bashrc
alias ls='ls --color=auto'
alias grep='grep --color=auto'
alias fgrep='fgrep --color=auto'
alias egrep='egrep --color=auto'

# Other aliases
alias open='xdg-open'

# Vi input mode
bindkey -v

# Enable CTRL-R search
bindkey ^R history-incremental-search-backward

# Enable correction of typos
setopt correct

# Disable globbing
unsetopt nomatch

# Store history
HISTSIZE=10000
SAVEHIST=10000
HISTFILE=~/.zsh_history

setopt append_history # Don't erase history
setopt extended_history # Add additional data to history like timestamp
setopt inc_append_history # Add immidiatly
setopt hist_find_no_dups # Don't show duplicates in search
setopt hist_ignore_space # Don't preserve spaces. You may want to turn it off
setopt hist_ignore_all_dups
setopt hist_reduce_blanks
setopt no_hist_beep # Don't beep

aa_256 () {
    ( x=`tput op` y=`printf %$((${COLUMNS}-6))s`;
    for i in {0..256};
    do
        o=00$i;
        echo -e ${o:${#o}-3:3} `tput setaf $i;tput setab $i`${y// /=}$x;
    done )
}

alias vim='vimx'

export GOPATH=~/dev/go
export PATH=$PATH:~/dev/go/bin
