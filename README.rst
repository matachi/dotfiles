dotfiles
========

Install prerequisite::

    $ sudo dnf install stow

Setup::

    $ cd ~
    $ git clone git@gitlab.com:matachi/dotfiles.git
    $ cd dotfiles
    $ stow git mpv tmux vim vscode zsh

