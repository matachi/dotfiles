" Rebind leader
let mapleader = "\<Space>"

nmap <Leader><Leader> V
vmap v <Plug>(expand_region_expand)
vmap <C-v> <Plug>(expand_region_shrink)

nnoremap <Leader>w :w<CR>

" Tabs
set textwidth=79  " lines longer than 79 columns will be broken
set shiftwidth=2  " operation >> indents 2 columns; << unindents 4 columns
set tabstop=2     " an hard TAB displays as 2 columns
set expandtab     " insert spaces when hitting TABs
set softtabstop=2 " insert/delete 2 spaces when hitting a TAB/BACKSPACE
set shiftround    " round indent to multiple of 'shiftwidth'
set autoindent    " align the new line indent with the previous line

set titleold= " Disable 'thanks for flying Vim' title

" Search
set ignorecase
set smartcase

" Hightlight
set incsearch
set hlsearch

" Spelling
"set spell spelllang=en_us

" Remember undo history when changing buffer
set hidden

" Switch on/off hlsearch
nnoremap <silent> <C-L> :nohlsearch<CR><C-L>

if has("persistent_undo")
  set undofile                " Save undo's after file closes
  set undodir=$HOME/.vim/undo " Where to save undo histories
  set undolevels=1000         " How many undos
  set undoreload=10000        " Number of lines to save for undo
endif

" set foldenable " Turn on folding
" set foldmethod=indent " Make folding indent sensitive
" set foldlevel=100 " Don't autofold anything (but I can still fold manually)
" set foldopen-=search " don't open folds when you search into them
" set foldopen-=undo " don't open folds when you undo stuff

" Show line numbers
set number
set relativenumber

" Color column @ 81
set cc=81

" Use system clipboard
"set clipboard=unnamedplus
autocmd VimLeave * call system("xsel -ib", getreg('+'))

set cursorline  " Highlight current line
set scrolloff=3 " Minimum lines to keep above and below cursor
set list        " Show invisible characters
"set listchars=tab:,.,trail:.,extends:#,nbsp:. " Highlight problematic whitespace

" Update time
set updatetime=100

" Show file name in title bar
set title

" Modify invisible characters
set listchars=extends:»,tab:▸\ ,trail:›

map <Leader>e :E<CR>

set ttimeout
set ttimeoutlen=50
let g:airline_left_sep=''
let g:airline_right_sep=''

set laststatus=2   " Always show the statusline
set showcmd
set wildmenu
set encoding=utf-8 " Necessary to show Unicode glyphs

" Show marks
"let showmarks_include = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ{}[]()'`.\""
let showmarks_include = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ{}()"
let g:showmarks_enable=1


set nojoinspaces

" Show syntax
syntax on

" Automatic indentation
filetype plugin indent on

" vim-plug https://github.com/junegunn/vim-plug
call plug#begin('~/.vim/plugged')

Plug 'Shougo/neobundle.vim'
" Plug 'vim-airline'
Plug 'tpope/vim-fugitive'
Plug 'tpope/vim-commentary'
Plug 'tpope/vim-surround'
Plug 'mattn/webapi-vim'
Plug 'mattn/gist-vim'
Plug 'docunext/closetag.vim'
Plug 'sjl/badwolf'
Plug 'robertmeta/nofrils'
" Plug 'vim-scripts/ShowMarks'
Plug 'kien/ctrlp.vim'
Plug 'jamessan/vim-gnupg'
Plug 'easymotion/vim-easymotion'

call plug#end()

set t_Co=256

colorscheme badwolf

" Properly set filetype
autocmd BufNewFile,BufReadPost *.md set filetype=markdown
autocmd BufNewFile,BufReadPost *.less set filetype=css

" CtrlP
set wildignore+=*/tmp/*,*.so,*.swp,*.zip,*/__pycache__/*,*.pyc,*/node_modules/*

let g:GPGExecutable="gpg2"
let g:GPGPreferArmor=1

let g:ctrlp_working_path_mode = 0

set synmaxcol=120
